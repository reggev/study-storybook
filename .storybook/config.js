import React from 'react';
import { configure } from '@storybook/react';
import { addParameters, addDecorator } from '@storybook/react';
import { themes } from '@storybook/theming';

addDecorator(storyFn => (
  <div
    style={{
      textAlign: 'center',
      width: '100vw',
      height: '100vh',
      justifyContent: 'center',
      alignItems: 'center',
      display: 'flex'
    }}>
    {storyFn()}
  </div>
));

addParameters({
  options: {
    theme: themes.light
  }
});

function loadStories() {
  const req = require.context('../src', true, /\.stories\.js$/);
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
