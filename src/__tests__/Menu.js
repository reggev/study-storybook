import React from 'react';
import { render as RTLRender, fireEvent } from '@testing-library/react';
import { Menu, Item } from '../components/Menu';

const render = (onChange = jest.fn()) => {
  const { getByTestId, getAllByTestId, ...rest } = RTLRender(
    <Menu onChange={onChange} iconName="">
      <Item>a</Item>
      <Item>b</Item>
      <Item>c</Item>
      <Item>d</Item>
    </Menu>
  );
  const menu = getByTestId('menu');
  return { menu, ...rest };
};

describe('Menu', () => {
  test('it should render without crashing', () => {
    const { menu } = render();
    expect(menu).toBeInTheDocument();
  });

  test('set tabIndex for items only when visible', () => {
    const { menu } = render();

    const getChildrenTabIndex = children =>
      Array.from(children)
        .filter(child => child.hasAttribute('tabindex'))
        .map(child => child.getAttribute('tabindex'));

    const hiddenState = getChildrenTabIndex(menu.childNodes);
    expect(hiddenState).toEqual(expect.arrayContaining(['0']));
    fireEvent.click(menu);
    const visibleState = getChildrenTabIndex(menu.childNodes);
    expect(visibleState).toEqual(expect.arrayContaining(['1']));
    fireEvent.click(menu);
    const reHiddenState = getChildrenTabIndex(menu.childNodes);
    expect(reHiddenState).toEqual(expect.arrayContaining(['0']));
  });
});
