import React from 'react';
import Modal from './../components/Modal';
import { render as RTLRender, waitForElement } from '@testing-library/react';

describe('modal', () => {
  test("render the modal with it's children", async () => {
    const content = 'I am a child';
    const onClose = jest.fn();
    const { getByText } = RTLRender(
      <Modal isDisplayed={true} onClose={onClose}>
        {content}
      </Modal>
    );
    const modalContent = await waitForElement(() => getByText(content));
    expect(modalContent).toBeInTheDocument();
  });
  test('not render the modal when it is hidden', async () => {
    const content = 'I am a child';
    const onClose = jest.fn();
    const { queryByText } = RTLRender(
      <Modal isDisplayed={false} onClose={onClose}>
        {content}
      </Modal>
    );
    expect(queryByText(content)).not.toBeInTheDocument();
  });
});
