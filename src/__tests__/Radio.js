import React, { useRef } from 'react';
import { render, fireEvent } from '@testing-library/react';
import { Radio, Option } from './../components/Radio';

describe('radio', () => {
  const handleChange = jest.fn();

  beforeEach(() => {
    handleChange.mockReset();
  });

  test('renders without crashing', () => {
    const { unmount } = render(
      <Radio onChange={handleChange}>
        <Option>option a</Option>
        <Option>option b</Option>
        <Option>option c</Option>
      </Radio>
    );
    unmount();
  });

  test.todo('run uncontrolled component with reading value ref');

  test.todo(
    'infer ids if not given any, expect Option.children to be used instead'
  );
  test.todo(
    'controlled component, expect onChange call when clicked, set active item on value change'
  );
});

const NonControllingComponent = () => {
  const radioRef = useRef(null);
  console.log(radioRef.current.value);
  return (
    <Radio ref={radioRef}>
      <Option>a</Option>
      <Option>b</Option>
      <Option>c</Option>
    </Radio>
  );
};
