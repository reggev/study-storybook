import React from 'react';
import { render as RTLRender, fireEvent } from '@testing-library/react';
import 'jest-dom/extend-expect';
import { Slider, Label, Preview } from '../components/Slider/index';

const WITH_PREVIEW = true;

const render = ({ sliderProps, labelProps }, withPreview) =>
  RTLRender(
    <Slider value={0} onChange={jest.fn()} {...sliderProps}>
      {withPreview && <Preview />}
      <Label {...labelProps} />
    </Slider>
  );

describe('Slider', () => {
  test('renders without crashing', () => {
    const { getByText } = render({}, WITH_PREVIEW);
    expect(getByText('0%')).toBeInTheDocument();
  });

  test('call click handler on slider click', () => {
    const clickHandler = jest.fn();
    const { getByTestId } = render(
      { sliderProps: { onChange: clickHandler } },
      WITH_PREVIEW
    );
    const slider = getByTestId('slider');
    fireEvent.click(slider);
    expect(clickHandler).toHaveBeenCalled();
    expect(slider).toBeInTheDocument();
  });

  test('renders custom format', () => {
    const { getByText } = render({
      sliderProps: {
        value: 0.2
      },
      labelProps: {
        format: value => `${value}$`
      }
    });
    expect(getByText('20$')).toBeInTheDocument();
  });

  test('custom range', () => {
    const { getByText } = render({
      sliderProps: {
        value: 0.5
      },
      labelProps: {
        min: 0,
        max: 50
      }
    });
    expect(getByText('25%')).toBeInTheDocument();
  });
});
