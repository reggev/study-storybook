import { renderHook } from 'react-hooks-testing-library';
import useSuspendedToggle from './../hooks/useSuspendedToggle';

describe('useSuspendedToggle custom hook', () => {
  test.each`
    isDisplayed | appear
    ${true}     | ${false}
    ${false}    | ${false}
  `('render initial isDisplayed: $isDisplayed', ({ isDisplayed }) => {
    const { result } = renderHook(() => useSuspendedToggle({ isDisplayed }));
    expect(result.current).toMatchSnapshot();
  });

  test('animate in on appear', async () => {
    const { result, waitForNextUpdate } = renderHook(() =>
      useSuspendedToggle({
        isDisplayed: true,
        appear: true
      })
    );
    const originalError = console.error;
    console.error = jest.fn();
    try {
      expect(result.current).toMatchSnapshot();
      await waitForNextUpdate();
      expect(result.current).toMatchSnapshot();
    } finally {
      console.error = originalError;
    }
  });

  test.each`
    initialState | startMethodCall | animationState | doneMethodCall | doneState
    ${false}     | ${'willEnter'}  | ${'ENTERING'}  | ${'didEnter'}  | ${'ENTERED'}
    ${true}      | ${'willExit'}   | ${'EXITING'}   | ${'didExit'}   | ${'EXITED'}
  `(
    'toggle from initialState $initialState',
    async ({
      initialState,
      startMethodCall,
      animationState,
      doneMethodCall,
      doneState
    }) => {
      const methods = {
        didEnter: jest.fn(),
        didExit: jest.fn(),
        willEnter: jest.fn(),
        willExit: jest.fn()
      };

      let isDisplayed = initialState;
      const { result, rerender, waitForNextUpdate } = renderHook(() =>
        useSuspendedToggle({
          isDisplayed,
          ...methods
        })
      );
      expect(result.current).toMatchSnapshot();
      const originalError = console.error;
      console.error = jest.fn();
      try {
        isDisplayed = !initialState;
        rerender();
        expect(result.current).toMatchSnapshot();
        expect(methods[startMethodCall]).toHaveBeenCalledWith(animationState);
        await waitForNextUpdate();
        expect(result.current).toMatchSnapshot();
        expect(methods[doneMethodCall]).toHaveBeenCalledWith(doneState);
      } finally {
        console.error = originalError;
      }
    }
  );

  test.each`
    initialState | prop            | enterDuration | exitDuration | shouldUseExitDuration
    ${false}     | ${'hasExited'}  | ${100}        | ${undefined} | ${false}
    ${true}      | ${'hasEntered'} | ${100}        | ${undefined} | ${false}
    ${false}     | ${'hasExited'}  | ${100}        | ${200}       | ${false}
    ${true}      | ${'hasEntered'} | ${100}        | ${200}       | ${true}
  `(
    'testing durations, if exitDuration is undefined enterDuration should be used',
    ({
      initialState,
      prop,
      enterDuration,
      exitDuration,
      shouldUseExitDuration
    }) => {
      jest.useFakeTimers();
      let isDisplayed = initialState;
      const { result, rerender } = renderHook(() =>
        useSuspendedToggle({
          isDisplayed,
          enterDuration,
          exitDuration
        })
      );
      expect(result.current[prop]).toBeTruthy();
      isDisplayed = !initialState;
      rerender();
      expect(setTimeout).toHaveBeenCalledWith(
        expect.any(Function),
        shouldUseExitDuration ? exitDuration : enterDuration
      );
    }
  );
});
