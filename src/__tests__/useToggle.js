import { renderHook, act } from 'react-hooks-testing-library';
import useToggle from './../hooks/useToggle';

const STATE = 0,
  TOGGLE_ON = 1,
  TOGGLE_OFF = 2,
  TOGGLE = 3;

describe('useToggle custom hook', () => {
  test('should initialize with default false value', () => {
    const { result } = renderHook(() => useToggle());
    expect(result.current[STATE]).toBeFalsy();
  });

  test('should toggle on', () => {
    const { result } = renderHook(() => useToggle());
    expect(result.current[STATE]).toBeFalsy();
    act(() => result.current[TOGGLE_ON]());
    expect(result.current[STATE]).toBeTruthy();
  });

  test('should toggle off', () => {
    const { result } = renderHook(() => useToggle(true));
    expect(result.current[STATE]).toBeTruthy();
    act(() => result.current[TOGGLE_OFF]());
    expect(result.current[STATE]).toBeFalsy();
  });

  test('should toggle on/off', () => {
    const { result } = renderHook(() => useToggle());
    expect(result.current[STATE]).toBeFalsy();
    act(() => result.current[TOGGLE]());
    expect(result.current[STATE]).toBeTruthy();
    act(() => result.current[TOGGLE]());
    expect(result.current[STATE]).toBeFalsy();
  });
});
