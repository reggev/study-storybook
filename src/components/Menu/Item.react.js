import React, { useContext, useMemo, useCallback } from 'react';
import PropTypes from 'prop-types';
import styles from './style.scss';
import Context from './context';

const degToRad = a => (a * Math.PI) / 180;

const itemStyle = (radians, radius) => ({
  transform: `
    translate(${Math.cos(radians) * radius * 100}px,
    ${Math.sin(radians) * radius * 100}px)
    scale(${radius})`,
  opacity: radius
});

const Item = ({ children }) => {
  const {
    isDisplayed,
    children: siblings,
    spreadAngle,
    startAngle,
    scale,
    handleChildClick
  } = useContext(Context);

  const idx = useMemo(
    () => siblings.findIndex(item => item.props.children === children),
    [siblings, children]
  );

  const handleClick = useCallback(() => handleChildClick(children), [
    children,
    handleChildClick
  ]);

  const getRadians = useCallback(
    () => degToRad((spreadAngle / siblings.length) * idx + startAngle),
    [spreadAngle, siblings, startAngle, idx]
  );

  return (
    <button
      tabIndex={isDisplayed ? 1 : 0}
      onClick={handleClick}
      className={styles.item}
      style={itemStyle(getRadians(), scale)}>
      {children}
    </button>
  );
};

Item.propTypes = {
  children: PropTypes.node.isRequired
};
export default Item;
