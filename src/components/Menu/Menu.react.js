import React, { useCallback } from 'react';
import { Motion, spring } from 'react-motion';
import PropTypes from 'prop-types';
import styles from './style.scss';
import { useToggle } from './../../hooks';
import Context from './context';

const springSettings = { stiffness: 150, damping: 12 };

const Menu = ({ onChange, iconName, spreadAngle, children, startAngle }) => {
  const [isDisplayed, , hide, toggle] = useToggle();

  const handleChildClick = useCallback(
    id => {
      hide();
      onChange(id);
    },
    [onChange, hide]
  );

  return (
    <Motion
      style={{
        rotation: spring(isDisplayed ? 135 : 0, springSettings),
        scale: spring(isDisplayed ? 1 : 0, springSettings)
      }}>
      {({ rotation, scale }) => (
        <Context.Provider
          value={{
            isDisplayed,
            children,
            scale,
            spreadAngle,
            startAngle,
            handleChildClick
          }}>
          <div onClick={toggle} className={styles.menu} data-testid="menu">
            {children}
            <i
              className={`${iconName} ${styles.center}`}
              style={{ transform: `rotateZ(${rotation}deg)` }}
            />
          </div>
        </Context.Provider>
      )}
    </Motion>
  );
};

Menu.propTypes = {
  onChange: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
  iconName: PropTypes.string.isRequired,
  spreadAngle: PropTypes.number,
  startAngle: PropTypes.number
};

Menu.defaultProps = {
  spreadAngle: 360,
  startAngle: 0
};

export default Menu;
