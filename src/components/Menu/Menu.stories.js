import React from 'react';
import icons from './../../icons';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { Menu, Item } from './';
import { withKnobs, number } from '@storybook/addon-knobs';

const stories = storiesOf('Menu', module);
stories.addDecorator(withKnobs);

const buttons = [
  icons.ios`cloud`,
  icons.ios`lightbulb-outline`,
  icons.ios`bolt`,
  icons.ios`camera`,
  icons.ios`chatboxes-outline`,
  icons.ios`analytics`,
  icons.ios`timer`,
  icons.ios`color-filter`
];

const range = n =>
  Array(n)
    .fill(0)
    .map((_, ii) => ii);

const amountOptions = {
  range: true,
  min: 2,
  max: 8,
  step: 1
};
const spreadAngleOptions = {
  range: true,
  min: 0,
  max: 360,
  step: 1
};

const startAngleOptions = {
  range: true,
  min: 0,
  max: 360,
  step: 1
};

stories.add('with knobs', () => (
  <Menu
    spreadAngle={number('spreadAngle', 145, spreadAngleOptions)}
    startAngle={number('startAngle', 120, startAngleOptions)}
    onChange={action('menu item selected')}
    iconName={icons.ios`gear`}>
    {range(number('#buttons', 3, amountOptions), {}).map(item => (
      <Item id={item} key={`button-${item}`}>
        <i className={buttons[item]} />
      </Item>
    ))}
  </Menu>
));
