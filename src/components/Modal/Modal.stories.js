import React from 'react';
import { storiesOf } from '@storybook/react';
import Modal from './';
import { useToggle } from './../../hooks';

const ModalController = ({ children }) => {
  const [isDisplayed, , , toggle] = useToggle(false);
  return (
    <>
      <button onClick={toggle}>toggle modal</button>
      <Modal isDisplayed={isDisplayed} onClose={toggle}>
        {children}
      </Modal>
    </>
  );
};

storiesOf('Modal', module).add('modal', () => (
  <ModalController>
    <h1>A modal!</h1>
    <p>this modal was rendered in it's own portal!</p>
  </ModalController>
));
