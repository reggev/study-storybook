import React, { useMemo } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import styles, { enterDuration, exitDuration } from './styles.scss';
import cs from 'classnames';
import { useSuspendedToggle } from './../../hooks';

let modalRoot = document.getElementById('modal-root');
if (!modalRoot) {
  modalRoot = document.createElement('div');
  modalRoot.setAttribute('id', 'modal-root');
  document.body.appendChild(modalRoot);
}

const dropEvent = e => e.stopPropagation();

const Modal = ({
  children,
  onClose,
  cssModule,
  isDisplayed,
  willEnter,
  willExit,
  didEnter,
  didExit,
  appear
}) => {
  const _enterDuration = useMemo(
    () =>
      parseInt(
        cssModule && cssModule.enterDuration
          ? cssModule.enterDuration
          : enterDuration
      ),
    [cssModule]
  );

  const _exitDuration = useMemo(
    () =>
      parseInt(
        cssModule && cssModule.exitDuration
          ? cssModule.exitDuration
          : exitDuration
      ),
    [cssModule]
  );

  const { isExiting, isEntering, hasExited } = useSuspendedToggle({
    isDisplayed,
    appear,
    enterDuration: _enterDuration,
    exitDuration: _exitDuration,
    willEnter,
    didEnter,
    willExit,
    didExit
  });

  if (hasExited) return null;
  return ReactDOM.createPortal(
    <div
      className={cs(
        styles.overlay,
        {
          [styles.overlayEnter]: isEntering,
          [styles.overlayExit]: isExiting
        },
        cssModule && {
          [cssModule.overlay]: true,
          [cssModule.overlayEnter]: isEntering,
          [cssModule.overlayExit]: isExiting
        }
      )}
      onClick={onClose}>
      <main
        className={cs(
          styles.modal,
          {
            [styles.modalEnter]: isEntering,
            [styles.modalExit]: isExiting
          },
          cssModule && {
            [cssModule.modal]: true,
            [cssModule.modalEnter]: isEntering,
            [cssModule.modalExit]: isExiting
          }
        )}
        onClick={dropEvent}>
        {children}
      </main>
    </div>,
    modalRoot
  );
};

Modal.propTypes = {
  isDisplayed: PropTypes.bool.isRequired,
  children: PropTypes.any.isRequired,
  onClose: PropTypes.func.isRequired,
  onModalClick: PropTypes.func,
  cssModule: PropTypes.shape({
    modalEnter: PropTypes.string,
    modalExit: PropTypes.string,
    overlayEnter: PropTypes.string,
    overlayExit: PropTypes.string,
    modal: PropTypes.string,
    overlay: PropTypes.string,
    enterDuration: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    exitDuration: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  }),
  willEnter: PropTypes.func,
  willExit: PropTypes.func,
  didEnter: PropTypes.func,
  didExit: PropTypes.func,
  appear: PropTypes.bool
};

export default Modal;
