import React, { useContext, useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Context } from './Radio.react';
import styles from './styles.scss';
import cs from 'classnames';

const Option = ({ id, children }) => {
  const { activeItem, setActiveItem } = useContext(Context);

  const _id = useMemo(() => id || children, [id, children]);

  const handleClick = useCallback(() => {
    setActiveItem(_id);
  }, [setActiveItem, _id]);

  return (
    <li
      onClick={handleClick}
      className={cs(styles.option, {
        [styles.active]: activeItem === _id
      })}>
      {children}
    </li>
  );
};

Option.propTypes = {
  children: PropTypes.PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    .isRequired,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

export default Option;
