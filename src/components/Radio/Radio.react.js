import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scss';
import { useRadio } from './../../hooks';

export const Context = React.createContext();

const Radio = ({ children, onChange, value }) => {
  const initialState =
    value || children[0].props.id || children[0].props.children;
  const [activeItem, setActiveItem] = useRadio(initialState, onChange, value);
  return (
    <Context.Provider value={{ activeItem, setActiveItem }}>
      <ul className={styles.container}>{children}</ul>
    </Context.Provider>
  );
};

Radio.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired
};

export default Radio;
