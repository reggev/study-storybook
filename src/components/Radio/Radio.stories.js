import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, number } from '@storybook/addon-knobs';
import { Radio, Option } from './';
import styles from './styles.scss';

const stories = storiesOf('Radio', module);
stories.addDecorator(withKnobs);

const handleChange = action('radio onChange');

const activeItem = {
  range: true,
  min: 1,
  max: 3,
  step: 1
};

stories
  .add('uncontrolled component', () => (
    <main className={styles.spinnersDemoContainer}>
      <Radio onChange={handleChange}>
        <Option id={1}>1</Option>
        <Option id={2}>2</Option>
        <Option id={3}>3</Option>
      </Radio>
    </main>
  ))
  .add('controlled component', () => (
    <main className={styles.spinnersDemoContainer}>
      <Radio
        onChange={handleChange}
        value={number('active item', 0, activeItem)}>
        <Option id={1}>1</Option>
        <Option id={2}>2</Option>
        <Option id={3}>3</Option>
      </Radio>
    </main>
  ))
  .add('inferred ids component', () => (
    <main className={styles.spinnersDemoContainer}>
      <Radio onChange={handleChange}>
        <Option>1</Option>
        <Option>2</Option>
        <Option>3</Option>
      </Radio>
    </main>
  ));
