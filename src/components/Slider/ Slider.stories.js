import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { Slider, Label, Preview } from './';
const sliderChange = action('slider onChange');

storiesOf('Slider', module)
  .add('basic', () => (
    <Slider value={0} onChange={sliderChange}>
      <Label />
    </Slider>
  ))

  .add('with hover preview', () => (
    <Slider value={0} onChange={sliderChange}>
      <Label />
      <Preview />
    </Slider>
  ))
  .add(
    'above 60%',
    () => (
      <Slider value={0.6} onChange={sliderChange}>
        <Label />
        <Preview />
      </Slider>
    ),
    {
      notes:
        'a slider with a value above the threshold should change the color of the text'
    }
  )
  .add(
    'custom format and range',
    () => (
      <Slider value={0.2} onChange={sliderChange}>
        <Label min={5} max={10} format={value => `${value}$`} />
        <Preview />
      </Slider>
    ),
    {
      notes: 'the format property enable custom text content'
    }
  );
