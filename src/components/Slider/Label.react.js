import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scss';
import context from './context';
const Label = ({ format, min, max, threshold, mainColor, secondaryColor }) => {
  const { value } = useContext(context);
  return (
    <span
      style={{ color: value <= threshold ? mainColor : secondaryColor }}
      className={styles.number}>
      {format(min + (max - min) * value)}
    </span>
  );
};

Label.defaultProps = {
  format: val => `${parseInt(val, 10)}%`,
  threshold: 0.56,
  min: 0,
  max: 100,
  mainColor: 'black',
  secondaryColor: 'white'
};
Label.propTypes = {
  format: PropTypes.func,
  min: PropTypes.number,
  max: PropTypes.number,
  threshold: PropTypes.number,
  mainColor: PropTypes.string,
  secondaryColor: PropTypes.string
};
export default Label;
