import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { translateX } from './utils';
import styles from './styles.scss';
import context from './context';

const Preview = ({ opacity }) => {
  const { isDisplayed, previewValue } = useContext(context);
  return (
    <div
      className={styles.preview}
      style={{
        transform: translateX(previewValue),
        opacity: isDisplayed ? opacity : 0
      }}
    />
  );
};

Preview.defaultProps = {
  opacity: 0.4
};

Preview.propTypes = {
  opacity: PropTypes.number
};

export default Preview;
