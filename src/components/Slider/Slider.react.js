import React, { useState, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scss';
import { useToggle } from './../../hooks';
import { translateX } from './utils';
import Context from './context';

const Slider = ({ value, onChange, min, max, children }) => {
  const [previewValue, setPreviewValue] = useState(value);
  const [isDisplayed, show, hide] = useToggle(false);
  const container = useRef(null);

  const getValue = useCallback(
    e =>
      parseFloat(
        (
          (e.clientX - container.current.offsetLeft) /
          container.current.offsetWidth
        ).toPrecision(2)
      ),
    [container]
  );

  const updatePreview = useCallback(e => setPreviewValue(getValue(e)), [
    getValue,
    setPreviewValue
  ]);

  const handleClick = useCallback(e => onChange(getValue(e)), [
    getValue,
    onChange
  ]);

  const handleScroll = useCallback(
    e => {
      e.preventDefault();

      let nextValue = value + (e.nativeEvent.wheelDelta > 0 ? 0.01 : -0.01);

      if (nextValue >= 1) nextValue = 1;
      else if (nextValue <= 0) nextValue = 0;

      onChange(parseFloat(nextValue.toFixed(2)));
    },
    [onChange, value]
  );

  return (
    <Context.Provider value={{ isDisplayed, previewValue, value }}>
      <div
        data-testid="slider"
        ref={container}
        onClick={handleClick}
        onMouseMove={updatePreview}
        onMouseEnter={show}
        onMouseLeave={hide}
        onWheel={handleScroll}
        className={styles.container}>
        <div
          className={styles.content}
          style={{ transform: translateX(value) }}
        />
        {children}
      </div>
    </Context.Provider>
  );
};

Slider.propTypes = {
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired
};

export default Slider;

/*
      <Preview previewValue={previewValue} isDisplayed={isDisplayed} />


*/
