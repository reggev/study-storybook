import Label from './Label.react';
import Slider from './Slider.react';
import Preview from './Preview.react';

export { Label, Slider, Preview };
