import React from 'react';
import { storiesOf } from '@storybook/react';
import Spinner from './';
import styles from './styles.scss';

storiesOf('Spinners', module).add('basic', () => (
  <main className={styles.spinnersDemoContainer}>
    <Spinner type="a" />
    <Spinner type="b" />
    <Spinner type="c" />
    <Spinner type="d" />
    <Spinner type="e" />
    <Spinner type="f" />
  </main>
));
