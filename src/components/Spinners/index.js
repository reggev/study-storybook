import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scss';

const Spinner = ({ type }) => <div className={styles[type]} />;

Spinner.propTypes = {
  type: PropTypes.oneOf(['a', 'b', 'c', 'd', 'e', 'f'])
};

export default Spinner;
