import React from 'react';
import { storiesOf } from '@storybook/react';
import { useInfiniteScroll } from '../hooks';
import styles from './styles.scss';

const List = () => {
  const [handleScroll, shouldFetchMore] = useInfiniteScroll(0.8);
  return (
    <ul
      className={styles.infiniteList}
      onScroll={handleScroll}
      style={{
        borderColor: shouldFetchMore ? '#f00' : 'inherit'
      }}>
      <h1>start scrolling</h1>
      {Array(15)
        .fill(0)
        .map((item, ii) => (
          <li className={styles.infiniteListItem} key={`list-item-${ii}`}>
            {ii}
          </li>
        ))}
    </ul>
  );
};
storiesOf('infinite scroll', module).add('basic', () => <List />);
