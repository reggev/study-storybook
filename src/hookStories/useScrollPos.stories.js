import React from 'react';
import { storiesOf } from '@storybook/react';
import { useScrollPos } from '../hooks';
import styles from './styles.scss';

const Element = () => {
  const [element, position] = useScrollPos();
  return (
    <h2 ref={element} style={{ opacity: Math.pow(position, 3) }}>
      I respond to my scroll position
    </h2>
  );
};

const App = () => (
  <main className={styles.scrollPosApp}>
    <h1 className={styles.scrollPosHeader}>scroll...</h1>
    <div>
      <Element />
    </div>
  </main>
);

storiesOf('ScrollPos', module).add('basic', () => <App />);
