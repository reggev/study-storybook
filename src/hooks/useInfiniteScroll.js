import { useCallback, useState } from 'react';
import _throttle from 'lodash.throttle';

const useInfiniteScroll = (threshold, throttle = 500) => {
  const [isOn, setOn] = useState(false);
  const _handleScroll = useCallback(
    _throttle(e => {
      const { scrollHeight, scrollTop, clientHeight } = e.target;
      setOn(scrollHeight - scrollTop <= clientHeight * (2 - threshold));
    }, throttle),
    [setOn, threshold]
  );
  const handleScroll = useCallback(e => _handleScroll(e), [_handleScroll]);
  return [handleScroll, isOn];
};

export default useInfiniteScroll;
