import { useState, useEffect } from 'react';

const useRadio = (initialState, callback, valueOverride) => {
  const [state, setState] = useState(initialState);
  useEffect(() => {
    valueOverride && setState(valueOverride);
  }, [valueOverride]);
  const handleChange = (nextValue = state) => {
    if (!valueOverride) setState(nextValue);
    typeof callback === 'function' && callback(nextValue);
  };
  return [state, handleChange];
};

export default useRadio;
