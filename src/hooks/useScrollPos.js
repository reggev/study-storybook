import { useRef } from 'react';
import { useScrollTop } from './';

const useScrollPos = throttle => {
  const scrollTop = useScrollTop(throttle);
  const element = useRef(null);
  if (!element.current) return [element, 0];
  const bounds = element.current && element.current.getBoundingClientRect();
  const windowSize = window.innerHeight;
  const visibleSpace = windowSize + (bounds.bottom - bounds.top);
  const itemPosition =
    (windowSize + scrollTop - bounds.top + bounds.height) / visibleSpace;
  return [element, itemPosition];
};

export default useScrollPos;
