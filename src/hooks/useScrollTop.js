import _throttle from 'lodash.throttle';

import { useEffect, useState } from 'react';

const useScrollTop = (throttle = 60) => {
  let [scrollTop, setScrollTop] = useState(0);
  useEffect(() => {
    const handleScroll = _throttle(
      e => setScrollTop(e.target.body.scrollTop),
      throttle
    );
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [throttle]);
  return scrollTop;
};

export default useScrollTop;
