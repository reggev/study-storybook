import { useEffect, useState, useCallback } from 'react';
import usePrevious from './usePrevious';

const useSuspendedToggle = ({
  isDisplayed,
  enterDuration,
  exitDuration,
  appear,
  didExit,
  didEnter,
  willExit,
  willEnter
}) => {
  const EXITING = 'EXITING',
    ENTERING = 'ENTERING',
    ENTERED = 'ENTERED',
    EXITED = 'EXITED';

  const [state, setState] = useState(isDisplayed ? ENTERED : EXITED);
  const startExit = useCallback(() => {
    setState(EXITING);
    willExit && willExit(EXITING);
    setTimeout(() => {
      setState(EXITED);
      didExit && didExit(EXITED);
    }, exitDuration || enterDuration);
  }, [setState, exitDuration, enterDuration, willExit, didExit]);

  const startEnter = useCallback(() => {
    setState(ENTERING);
    willEnter && willEnter(ENTERING);
    setTimeout(() => {
      setState(ENTERED);
      didEnter && didEnter(ENTERED);
    }, enterDuration || exitDuration);
  }, [setState, exitDuration, enterDuration, willEnter, didEnter]);

  const wasDisplayed = usePrevious(isDisplayed);
  const isAnimating = state === ENTERING || state === EXITING;

  // -- handle appear
  if (
    typeof wasDisplayed === 'undefined' &&
    appear &&
    isDisplayed &&
    !isAnimating
  ) {
    startEnter();
  }

  // -- handle prop change
  useEffect(() => {
    if (isAnimating) return;
    if (wasDisplayed !== isDisplayed) {
      if (!isDisplayed && state === EXITED) return;
      if (isDisplayed && state === ENTERED) return;
      isDisplayed ? startEnter(200) : startExit(200);
    }
  }, [
    isDisplayed,
    state,
    startEnter,
    startExit,
    wasDisplayed,
    appear,
    isAnimating
  ]);

  return {
    isExiting: state === EXITING,
    isEntering: state === ENTERING,
    hasExited: state === EXITED,
    hasEntered: state === ENTERED,
    isAnimating,
    state
  };
};

export default useSuspendedToggle;
