import { useState } from 'react';

const useToggle = (initialState = false) => {
  const [state, setState] = useState(initialState);
  const setOn = () => setState(true);
  const setOff = () => setState(false);
  const toggle = () => setState(!state);

  return [state, setOn, setOff, toggle];
};

export default useToggle;
