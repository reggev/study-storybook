import icons from './ionicons.scss';

export default {
  ios: icon => icons[`ion-ios-${icon}`],
  android: icon => icons[`ion-android-${icon}`],
  social: icon => icons[`ion-social-${icon}`],
  ion: icon => icons[`ion-${icon}`]
};

export { icons };
