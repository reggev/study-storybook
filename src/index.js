import Menu from './components/Menu';
import Slider from './components/Slider';
import Modal from './components/Modal';
import Spinner from './components/Spinners/';

import {
  useInfiniteScroll,
  usePrevious,
  useScrollPos,
  useScrollTop,
  useSuspendedToggle,
  useToggle
} from './hooks/';

export {
  useInfiniteScroll,
  usePrevious,
  useScrollPos,
  useScrollTop,
  useSuspendedToggle,
  useToggle,
  Menu,
  Slider,
  Modal,
  Spinner
};
